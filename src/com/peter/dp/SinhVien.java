package com.peter.dp;

@lombok.AllArgsConstructor
@lombok.Data
public class SinhVien {
	private String MaSinhVien;
	private String HoTen;
	private double DiemTrungBinh;
}
